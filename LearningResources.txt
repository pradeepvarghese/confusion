https://www.w3.org/TR/css-flexbox/

https://css-tricks.com/snippets/css/a-guide-to-flexbox/

https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties

http://tutorialzine.com/2016/11/boostrap-4-regular-vs-flex-grid/

http://blog.codeply.com/2016/04/06/how-the-bootstrap-grid-really-works/

http://www.helloerik.com/the-subtle-magic-behind-why-the-bootstrap-3-grid-works

http://johnpolacek.github.io/scrolldeck.js/decks/responsive/

http://blog.teamtreehouse.com/beginners-guide-to-responsive-web-design

http://blog.teamtreehouse.com/modern-field-guide-responsive-web-design

Ideation Report Template
Project Title
1. Introduction
A brief introduction to your website idea. State the goals of the project.
The values / benefits (tangible and intangible) this application can bring to a company/organization/end-user.
2. Expected List of Features
A brief list of features that you expect your website to support.
Brief justifications for including these features.
3. Market Survey
Do a survey of the Web to find about five web sites that might have similar ideas as yours.
Briefly compare the features of these applications with your application idea.
4. References
Give references to any material / websites / books etc. relevant to your application idea
Give the links to the websites relevant to your idea, that you listed in the section above.

https://en.wikipedia.org/wiki/Ideation_(creative_process)


http://getbootstrap.com/docs/4.0/components/navbar/

http://getbootstrap.com/docs/4.0/components/breadcrumb/

https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA

http://sixrevisions.com/usabilityaccessibility/information-architecture-101-techniques-and-best-practices/

http://webdesignfromscratch.com/website-architecture/ia-models/

http://www.steptwo.com.au/papers/kmc_whatisinfoarch/

http://www.webmonkey.com/2010/02/Information_Architecture_Tutorial/

http://www.hongkiat.com/blog/navigation-design-ideas-inspiration/

https://blog.kissmetrics.com/common-website-navigation-mistakes/

http://www.webdesignerdepot.com/2014/01/3-reasons-we-should-stop-using-navigation-bars/

http://www.hongkiat.com/blog/breadcrumb-navigation-examined-best-practices-examples/

http://blog.woorank.com/2014/11/breadcrumb-navigation-guide/

http://vanseodesign.com/web-design/icon-fonts/

https://css-tricks.com/examples/IconFont/

http://fontawesome.io/

http://fontawesome.io/get-started/

http://lipis.github.io/bootstrap-social/

http://www.sitepoint.com/final-nail-icon-fonts-coffin/

http://www.sitepoint.com/final-nail-icon-fonts-coffin/

http://getbootstrap.com/docs/4.0/components/buttons/

http://getbootstrap.com/docs/4.0/components/button-group/

http://getbootstrap.com/docs/4.0/components/forms/

http://davidwalsh.name/html5-buttons

https://css-tricks.com/use-button-element/

http://getbootstrap.com/docs/4.0/components/badge/

http://getbootstrap.com/docs/4.0/components/alerts/

http://getbootstrap.com/docs/4.0/components/progress/

Wireframing, Mockups and UI Design

https://wireframe.cc/

https://moqups.com/

https://moqups.com/

https://proto.io/

http://framerjs.com/

http://www.creativebloq.com/wireframes/top-wireframing-tools-11121302

http://www.webdesign-inspiration.com/

http://www.adobe.com/products/experience-design.html

https://onextrapixel.com/free-bootstrap-wireframing-set-for-powerpoint/

http://expo.getbootstrap.com/

http://showcase.ionicframework.com/

http://www.jjg.net/ia/visvocab/

http://www.jjg.net/ia/visvocab/

http://www.amazon.com/The-Elements-User-Experience-User-Centered/dp/0321683684/ref=pd_cp_14_1?ie=UTF8&refRID=0RXJWKFHY0TNF5QM2764

http://getbootstrap.com/docs/4.0/getting-started/javascript/

http://getbootstrap.com/docs/4.0/getting-started/javascript/#data-attributes

http://getbootstrap.com/docs/4.0/getting-started/javascript/#programmatic-api

http://getbootstrap.com/docs/4.0/components/navs/

http://getbootstrap.com/docs/4.0/components/navs/#tabs

http://getbootstrap.com/docs/4.0/components/navs/#pills

http://getbootstrap.com/docs/4.0/components/navs/#javascript-behavior

http://getbootstrap.com/docs/4.0/components/collapse/

http://getbootstrap.com/docs/4.0/components/collapse/#accordion-example

http://getbootstrap.com/docs/4.0/components/tooltips/

http://getbootstrap.com/docs/4.0/components/popovers/

http://getbootstrap.com/docs/4.0/components/modal/

http://getbootstrap.com/docs/4.0/components/carousel/

http://getbootstrap.com/docs/4.0/components/buttons/#checkbox-and-radio-buttons

http://getbootstrap.com/docs/4.0/components/modal/

http://getbootstrap.com/docs/4.0/components/modal/

http://getbootstrap.com/docs/4.0/components/carousel/#methods

http://jquery.com/

http://www.w3schools.com/jquery/default.asp

http://lesscss.org/

http://sass-lang.com/guide

https://scotch.io/tutorials/getting-started-with-less

https://scotch.io/tutorials/getting-started-with-less

https://www.npmjs.com/package/less

https://www.npmjs.com/package/node-sass

https://css-tricks.com/why-npm-scripts/

https://www.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/

https://webdesign.tutsplus.com/series/the-command-line-for-web-design--cms-777

https://github.com/Qard/onchange

https://github.com/keithamus/parallelshell

https://github.com/isaacs/rimraf

https://github.com/isaacs/rimraf

https://github.com/isaacs/rimraf

https://github.com/nelsyeung/usemin-cli

https://github.com/nelsyeung/usemin-cli

https://github.com/nelsyeung/usemin-cli

https://github.com/nelsyeung/usemin-cli

GREAT RESOURCE for learning PHP  (Others too like OOP, MVC, architecture...etc)
http://tonymarston.net/


http://gruntjs.com/

http://www.sitepoint.com/writing-awesome-build-script-grunt/

http://anders.janmyr.com/2014/01/clean-grunt.html

http://gruntjs.com/configuring-tasks#globbing-patterns

https://webdesign.tutsplus.com/tutorials/the-command-line-for-web-design-automation-with-grunt--cms-23454

http://gulpjs.com/

http://www.sitepoint.com/introduction-gulp-js/

https://markgoodyear.com/2014/01/getting-started-with-gulp/

http://www.smashingmagazine.com/2014/06/building-with-gulp/

https://webdesign.tutsplus.com/tutorials/the-command-line-for-web-design-automation-with-gulp--cms-23642

https://en.wikipedia.org/wiki/Minification_(programming)

http://lisperator.net/uglifyjs/

http://jshint.com/

http://juristr.com/blog/2014/08/node-grunt-yeoman-bower/

https://www.dbswebsite.com/blog/2015/02/24/the-advantages-of-using-task-runners/

https://medium.com/@preslavrachev/gulp-vs-grunt-why-one-why-the-other-f5d3b398edc4

http://blog.keithcirkel.co.uk/why-we-should-stop-using-grunt/

https://medium.freecodecamp.com/why-i-left-gulp-and-grunt-for-npm-scripts-3d6853dd22b8


